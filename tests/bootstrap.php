<?php
include(__DIR__.'/../vendor/autoload.php');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define('CONNECTION_STRING', 'mysql:127.0.0.1#13306:test:webuser:webpasswd:1');

// delete database test
call_user_func(function () {
    \Spinit\Datamanager\DataManagerFactory::getDataManager(CONNECTION_STRING, false)->exec('DROP DATABASE IF EXISTS test');
});