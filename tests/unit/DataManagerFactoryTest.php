<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\Test\Unit;

use PHPUnit\Framework\TestCase;
use Spinit\Datamanager\DataManagerFactory;

/**
 * Description of InitTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataManagerFactoryTest extends TestCase
{
    public function testInit()
    {
        $mysqlManager = DataManagerFactory::getDataManager('mysql:uno:due');
        $this->assertInstanceOf("\\Spinit\\Datamanager\\PDO\\Managers\\Mysql\\Manager", $mysqlManager);
        $this->assertEquals(['mysql', 'uno', 'due'], $mysqlManager->getInfo());
    }
    
    public function testSerialize()
    {
        DataManagerFactory::setTypeSerializer('mysql', 'type1', function($value) { return 'TEST = '.$value; });
        $this->assertEquals('TEST = OK', DataManagerFactory::getValue('mysql', 'type1', 'OK'));
        $this->assertEquals('OK', DataManagerFactory::getValue('mysql', 'other', 'OK'));
        $this->assertEquals('TEST', DataManagerFactory::getValue('BHO', 'other', 'TEST'));
    }
    
    public function testSerializeData()
    {
        $this->assertEquals('2018-12-01 01:01:01', DataManagerFactory::getValue('mysql', 'datetime', '01/12/2018 01:01:01'));
        $this->assertEquals('01/12/2018 01:01:01', DataManagerFactory::getValue('BHO', 'datetime', '01/12/2018 01:01:01'));
    }
}
