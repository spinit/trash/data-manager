<?php
namespace Spinit\Datamanager\Test\Functional\PDOManager;

use Spinit\Datamanager\DataManagerFactory;
use PHPUnit\Framework\TestCase;
use Spinit\Datamanager\PDO\PDOManager;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datamanager\DatastructConverter\Mysql2DataStruct;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDOManagerTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDOManagerTest extends TestCase
{
    /**
     *
     * @var PDOManager
     */
    private $obj = null;
    public function setUp()
    {
        $this->obj = DataManagerFactory::getDataManager(CONNECTION_STRING);
    }
    public function testConnection()
    {
        $this->obj->connect();
        $this->obj->drop('prova');
        $this->assertFalse($this->obj->check('prova'));
    }
    public function testCreateTable()
    {
        $ds = new DataStruct('prova');
        $ds->addField(new Field('id'))->set('type', 'varchar')->set('size', '20');
        $this->obj->align($ds);
        $this->obj->insert('prova', ['id'=>'10']);
        $res = $this->obj->load('select * from prova')->first();
        $this->assertEquals(['id'=>'10'], $res);
    }
    public function testMysqlDump()
    {
        $dsList = new \Spinit\Datastruct\Converter\MysqldumpXml2DataStruct(__DIR__.'/xml/Test-D1.xml');
        $this->obj->setParam('xmlroot', __DIR__.'/xml');
        foreach($dsList->getDataStructList() as $ds) {
            $this->obj->align($ds);
        }
        $this->assertTrue($this->obj->getLib()->checkTable('osy_user'));
        
    }
    public function testAddColumn()
    {
        $dsList = new Mysql2DataStruct(CONNECTION_STRING);
        $dsUser = $dsList->getDataStruct('osy_user');
        $this->assertEquals('20', $dsUser->getField('tel_2')->get('size'));
        
        $ds = new DataStruct('osy_user');
        $ds->addField(new Field('test_field'))->set('type', 'varchar')->set('size', '1');
        $ds->addField(new Field('tel_2'))->set('type', 'varchar')->set('size', '50');
        define('stop', 1);
        $this->obj->align($ds);
        
        $dsList = new Mysql2DataStruct(CONNECTION_STRING);
        
        $dsUser = $dsList->getDataStruct('osy_user');
        $this->assertEquals('1', $dsUser->getField('test_field')->get('size'));
        $this->assertEquals('50', $dsUser->getField('tel_2')->get('size'));
    }
    
    public function testParams()
    {
        $ret = $this->obj->load("
            select  case when /*a*/ 1 = {{a}} then 1 else 0 end as val,
                    case when /*b*/ 1 = {[b.r]} then 2 else 0 end as bal,
                    case when /*c*/ null = {{c}} then 3 else 0 end cal,
                    case when /*t*/ '' = {{t}} then 4 else 0 end tal
            ", ['a'=>1, 'c'=>null], ['b'=>['r'=>'1']])->first();
        
        
        $this->assertEquals('1', $ret['val']);
        $this->assertEquals('2', $ret['bal']);
        $this->assertEquals('3', $ret['cal']);
        $this->assertEquals('4', $ret['tal']);
        
        $cmd = $this->obj->getCommandLast();
        $this->assertContains("/*a*/ 1 = '1'", $cmd['sql']);
        $this->assertContains("/*b*/ 1 = '1'", $cmd['sql']);
        $this->assertContains("/*c*/ null  IS NULL", $cmd['sql']);
        $this->assertContains("/*t*/ '' = ''", $cmd['sql']);
    }
    
}
