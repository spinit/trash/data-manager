<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\Helper;

use Spinit\Util\Error\NotFoundException;

/**
 * Description of XmlDataRecordParser
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class XmlDataRecordParser
{
    private $path = '';
    private $callback = null;
    
    private $cdata = '';
    private $inDataRecord = false;
    private $record = false;
    
    public function __construct($path, $callback)
    {
        $this->path = $path;
        $this->callback = $callback;
    }
    
    public function parse()
    {
        $handle = false;
        try {
            $handle = @fopen($this->path, "r");
        } catch (\Exception $ex) {
            throw new NotFoundException('File not found : '.$this->path);
        }
        // open xml file
        if ($handle) {
            $parser = xml_parser_create(); 

            xml_set_element_handler($parser, array($this, "tagStartElements"), array($this, "tagEndElements"));
            xml_set_character_data_handler($parser, array($this, "tagCharacterData"));
            
            while($data = fread($handle, 4096)) { // read xml file
               xml_parse($parser, $data);  // start parsing an xml document 
            }
            
            fclose($handle);
            xml_parser_free($parser); // deletes the parser
        }
    }
    
    
    // Called to this function when tags are opened 
    private function tagStartElements($parser, $name, $attr)
    {
        switch($name) {
            case 'DATA_RECORD':
                $this->inDataRecord = true;
                $this->record = [];
                break;
        }
        $this->cdata = '';
    }

    // Called to this function when tags are closed 
    private function tagEndElements($parser, $name)
    {
        switch($name) {
            case 'DATA_RECORD':
                $this->inDataRecord = false;
                if ($this->callback) {
                    call_user_func($this->callback, $this->record);
                }
                break;
            default:
                if ($this->inDataRecord) {
                    $this->record[strtolower($name)] = $this->cdata;
                }
        }
    }

    // Called on the text between the start and end of the tags
    private function tagCharacterData($parser, $data)
    {
        $this->cdata .= $data;
    }
}
