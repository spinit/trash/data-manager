<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager;

use Spinit\Datamanager\DataSetInterface;

/**
 * Description of DataSetArray
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSetArray extends \ArrayIterator implements DataSetInterface
{
    public function close() {
        
    }

    public function getMetadata($type = '') {
        
    }

    public function isOpen() {
        
    }

    public function position() {
        
    }

    public function rowCount()
    {
        return $this->count();
    }

}
