<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager;

use Spinit\Util\Error\NotFoundException;
use Spinit\Util;

/**
 * Description of DataManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataManagerFactory
{
    private static $listManager = [];
    private static $listConnection = [];
    
    private static $listSerializer = [];
    private static $user;
    
    public static function setDataManager($type, $manager)
    {
        foreach(Util\asArray($type, ',') as $name) {
            self::$listManager[$name] = $manager;
        }
    }
    
    public static function getDataManager($connectionString, $save = true)
    {
        $connectionKey = md5($connectionString);
        if (array_key_exists($connectionKey, self::$listConnection)) {
            return self::$listConnection[$connectionKey];
        }
        $info = explode(':', $connectionString);
        $connection = Util\getInstance(
            Util\arrayGet(
                self::$listManager,
                $info[0],
                Util\throwError("Tipo sconosciuto : ".$info[0], "Spinit:Util:Error:NotFoundException")
            ),
            $connectionString
        );
        $connection->setParam('userTrace', self::$user);
        if ($save) {
            self::$listConnection[$connectionKey] = $connection;
        }
        return $connection;
    }
    
    public static function setTypeSerializer($typeConnection, $typeData, $callable)
    {
        foreach(Util\asArray($typeConnection, ',') as $name) {
            foreach(Util\asArray($typeData, ',') as $type) {
                self::$listSerializer[$name][$type] = $callable;
            }
        }
    }
    
    public static function getValue($typeConnection, $type, $value)
    {
        // viene cercata la funzione che prepara il valore da dover memorizzare
        $serializer = Util\arrayGet(
            self::$listSerializer,
            [$typeConnection, $type],
            function () {
                return function ($val) {
                    return $val;
                };
            }
        );
        
        return $serializer($value);
    }
    
    public function setUserTrace($user)
    {
        self::$user = $user;
    }
}
