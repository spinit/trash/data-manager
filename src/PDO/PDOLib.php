<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO;

use Spinit\Datastruct\Field;
use Spinit\Datastruct\DataStructInterface;
use Spinit\Datastruct\Index;
use Spinit\Datamanager\Helper\XmlDataRecordParser;

use Webmozart\Assert\Assert;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

use Spinit\Datamanager\PDO\Command;

/**
 * Description of PDOLib
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class PDOLib
{
    private $manager = null;
    private $normalizeNull = true;
    
    public function setManager(PDOManager $manager)
    {
        $this->manager = $manager;
    }
    public function getManager()
    {
        return $this->manager;
    }
    
    public function dropTable($name)
    {
        return $this->getManager()
            ->exec("DROP TABLE IF EXISTS ".$name);
    }
    public function getSql($query)
    {
        if (is_string($query)) {
            return $query;
        }
        if (isset($query['query'])) {
            $query = $query['query'];
        }
        // query specifica per il tipo database utilizzato
        $type = $this->getManager()->getInfo(0);
        $sql  = Util\arrayGet($query, $type);
        if (!$sql) {
            // query generica
            $sql  = Util\arrayGet($query, 'sql');
        }
        Assert::notEmpty($sql, "Query not found");
        return $sql;
    }
    /**
     * Ridefinendo il metodo è possibile restituire un altro gestore per lo scorrimento del risultato
     */
    public function getDataSet($rs)
    {
        return new PDODataSet($rs);
    }
    abstract public function checkTable($table);
    abstract protected function getDataStructList();
    
    public function isSameField(Field $field0, Field $field1)
    {
        $data0 = [$this->getTypeField(null, $field0, false, null), $field0->get('notnull', true)];
        $data1 = [$this->getTypeField(null, $field1, false, null), $field1->get('notnull', true)];
        return $data0 == $data1;
    }
    
    public function getDataStruct($resource)
    {
        $dl = $this->getDataStructList();
        $ds = $dl->getDataStruct($resource);
        try {
            $dl->getDataStruct($resource.'__');
            $ds->setParam('trace', '1');
        } catch (\Exception $e) {
            // pass
        }
        return $ds;
    }

    public function createTableField($struct, Field $field, $trace, $addCommand, $isUpdate = false)
    {
        $type = $this->getTypeField($struct, $field, $trace, $addCommand, $isUpdate);
        $str = "    `{$field->getName()}` {$type}";
        return $str;
    }
    protected function getTypeField($struct, Field $field, $trace, $addCommand, $isUpdate = false)
    {
        $type = $field->get('type');
        $size = $field->get('size', '');
        if ($type == 'varchar' and !$size) {
            $size = '50';
        } 
        if ($size) {
            $type .= "({$size})";
        }
        if ($field->get('unsigned', false)) {
            $type .= ' unsigned';
        }
        return $type;
    }
    public function addTableField($struct, Field $field, $trace, $addCommand)
    {
        return "ALTER TABLE {$struct->getName()} ADD COLUMN ".$this->createTableField($struct, $field, $trace, $addCommand);
    }
    
    public function updateTableField($struct, Field $field, $trace, $addCommand)
    {
        return "ALTER TABLE {$struct->getName()} MODIFY COLUMN ".$this->createTableField($struct, $field, $trace, $addCommand, true);
    }
    
    abstract public function createTableIndex($resource, $name, $fieldList = array(), $type = 'BTREE');
    
    abstract public function updateTableIndex(DataStructInterface $struct, $name, Index $index);

    /**
     * La normalizzazione della query avviene in due passi
     * - viene chiesto al database se occorre modificare lo script individuato (es. effettuare delle sostituzioni
     *   con dei placeolder)
     * - vengono sostituiti nella query stessa i parametri che sono presenti nella forma {{@?parametro}}
     * @param type $sqlraw
     * @param type $params
     * @return type
     */
    public function normalizeExec($sqlraw, $params, $args = array(), $info = array(), $vars = array())
    {
        $args = $args ? $args : [];
        $params = $params?:[];
        $sqlraw = $this->getSql($sqlraw);
        // sostituzione nome database
        $sql = $this->getManager()->trigger('normalizeQuery', $sqlraw);
        if (!$sql or !is_string($sql)) {
            $sql = $sqlraw;
        }
        // sostituzione parametri nella query
        preg_match_all('/\{\{([^ ]+)\}\}/', $sql, $matches);
        $unset = [];
        foreach ($matches[1] as $k => $name) {
            $sql = $this->normalizeValue($name, $params, $matches[0], $k, $sql, $unset);
        }
        foreach($unset as $v) {
            unset($params[$v]);
        }
        $unset = [];
        preg_match_all('/\{\[([^ ]+)\]\}/', $sql, $matches);
        foreach ($matches[1] as $k => $name) {
            $sql = $this->normalizeValue($name, $args, $matches[0], $k, $sql, $unset);
        }
        foreach($unset as $v) {
            unset($args[$v]);
        }
        $unset = [];
        preg_match_all('/\{\(([^ ]+)\)\}/', $sql, $matches);
        foreach ($matches[1] as $k => $name) {
            $sql = $this->normalizeValue($name, $info, $matches[0], $k, $sql, $unset);
        }
        foreach($unset as $v) {
            unset($info[$v]);
        }
        $unset = [];
        preg_match_all('/\{\|([^ ]+)\|\}/', $sql, $matches);
        foreach ($matches[1] as $k => $name) {
            $sql = $this->normalizeValue($name, $vars, $matches[0], $k, $sql, $unset);
        }
        foreach($unset as $v) {
            unset($vars[$v]);
        }
        $unset = [];
        if ($this->normalizeNull) {
            // sostituzione valorizzazioni NULL
            $sql = str_replace(array("!= NULL", "<> NULL"), array(' IS NOT NULL', ' IS NOT NULL'), $sql);
            $sql = str_replace(array("= NULL"), array(' IS NULL'), $sql);
        } else {

            // una volta usato ... si reimposta al valore naturale
            $this->normalizeNull = true;
        }
        // se nei parametri ci sono rimasti elementi array ... vengono tolti
        foreach($params as $key => $value) {
            if (is_array($value)) {
                unset($params[$key]);
            }
        }
        // viene restituita la query pronta per l'esecuzione con l'insieme di parametri che sono affidati al driver del DB
        return array($sql, $params);
    }
    
    protected function normalizeValue($field, &$params, $matches, $k, $sql, &$unset)
    {
        // non vuole gli apici finali?
        $strip = ($field{0} == ':');
        $field = $strip ? substr($field, 1) : $field;
        // è un esadecimale?
        $at = ($field{0} == '@');
        $field = $at ? substr($field, 1) : $field;
        // è una lista di valori?
        $lst = ($field{0} == '+');
        $field = $lst ? substr($field, 1) : $field;
        foreach(explode('.', $field) as $ff) {
            if (array_key_exists($ff, $params)) {
                if ($params[$ff] === null) {
                    $value = null;
                    break;
                }
                if (is_array($params[$ff])) {
                    $value = $params[$ff];
                    $params = &$params[$ff];
                    continue;
                }
                if ($at) {
                    $value = $this->unhex($params[$ff]);
                } else {
                    $value = $this->getManager()->quote($params[$ff]);
                    if ($strip) {
                        $value = substr($value, 1, -1);
                    }
                }
                if (!in_array($ff, $unset)) {
                    $unset[] = $ff;
                }
            } else {
                // se un valore non è definito allora viene impostato come stringa vuota
                // per metterlo a null occorre impostarlo con il valore [null]
                $value = "''";
            }
            break;
        }
        if ($value === null) {
            if ($lst) {
                $value = "''";
            } else {
                $value = 'NULL';
            }
        } else if (is_array($value)) {
            if ($lst) {
                $ll = [];
                foreach($value as $val) {
                    if (is_array($val)) {
                        throw new \Exception('Tipo dato errato : '.json_encode($value, JSON_UNESCAPED_UNICODE));
                    }
                    if ($at) {
                        $ll []= $this->unhex($val);
                    } else {
                        $ll []= $this->getManager()->quote($val);
                    }
                }
                if (count($ll)) {
                    $value = implode(', ', $ll);
                } else {
                    $value = "''";
                }
            } else {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }
        }
        $sql = str_replace($matches[$k], $value, $sql);
        return $sql;
    }
    
    /**
     * Definisce come comportarsi in fase di importazione quando un campo ha una stringa nulla
     * @param type $struct
     * @return type
     */
    public function makeNullStruct($struct)
    {
        $nullConf = [];
        foreach ($struct->getFieldList() as $field) {
            if (in_array($field->get('type'), ['varchar', 'text'])) {
                $nullConf[$field->getName()] = '';
            } else {
                $nullConf[$field->getName()] = null;
            }
        }
        return $nullConf;
    }
    public function loadXmlData($root, $struct)
    {
        // struttura che mappa come interpretare le stringhe vuote in un campo
        $nullConf = $this->makeNullStruct($struct);
        try {
            $iter = new XmlDataRecordParser("{$root}/{$struct->getName()}.xml", function ($record) use ($struct, $nullConf) {
                foreach($record as $k => $v) {
                    if ($v === '') {
                        $record[$k] = $nullConf[$k];
                    }
                }
                $this->getManager()->insert($struct->getName(),  $record);
            });
            $iter->parse();
        } catch (NotFoundException $e) {
            // nothing
        }
    }
    
    public function notNormalizeNull()
    {
        $this->normalizeNull = false;
    }
    /**
     * Determina se un valore deve essere inserito direttamente nella query o viene passato come argomento
     * @param type $field
     * @param type $data
     * @return type
     */
    public function formatFieldValue($field, &$data, $pfx = '')
    {
        $column = $field->getName();
        if (in_array($field->get('type'), ['uuid','binary'])) {
            $value = $data[$pfx.$column];
            if (is_array($value)) {
                $value = array_shift($value);
            }
            if (strlen($value)) {
                $value = $this->unhex($value);
                unset($data[$pfx.$column]);
                return $value;
            }
            $data[$pfx.$column] = null;
        }
        if ($data[$pfx.$column] === null) {
            unset($data[$pfx.$column]);
            return 'NULL';
        }
        return ':'.$pfx.$column;
    }
    public function unhex($value)
    {
        return 'UNHEX('.$this->getManager()->quote($value).')';
    }
    public function formatFieldSelect($field)
    {
        $column = $field->getName();
        if (in_array($field->get('type'), ['uuid','binary'])) {
            return 'upper(HEX('.$column.')) as '.$column;
        }
        return $column;
    }
    
    public function getNowDataTime()
    {
        return date('Y-m-d H:i:s');
    }
    
    /**
     * Se la risorsa esiste viene impostato il cmando di aggiornamento struttura .. altrimenti di creazione
     * @param DataStructInterface $struct
     * @param type $trace
     * @return type
     */
    public function getTableUpdater(DataStructInterface $struct)
    {
        $resource = $struct->getName();
        if ($this->checkTable($resource)) {
            return Util\getInstance(
                __NAMESPACE__.'\\Command\\UpdateTable',
                $this,
                $this->getDataStruct($resource), 
                $struct);
        } 
        $class = __NAMESPACE__.'\\Command\\CreateTable';
        return Util\getInstance($class, $this, $struct);
    }
    
    /**
     * Aggiornamento o creazione per la tabella trace
     * @param DataStructInterface $struct
     * @return type
     */
    public function getTableTraceUpdater(DataStructInterface $struct)
    {
        $resource = $struct->getName().'__';
        if ($this->checkTable($resource)) {
            $class = __NAMESPACE__.'\\Command\\UpdateTraceTable';
            return Util\getInstance($class, $this, $this->getDataStruct($resource), $struct);
        } 
        $class = __NAMESPACE__.'\\Command\\CreateTraceTable';
        return Util\getInstance($class, $this, $struct);
    }
}