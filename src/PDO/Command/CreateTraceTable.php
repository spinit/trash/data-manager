<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Command;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datastruct\Field;
use Spinit\Datamanager\PDO\Command;

use Spinit\Datamanager\PDO\PDOLib;

/**
 * Description of CreateTable
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CreateTraceTable extends Command
{
    private $fieldsTrace;
    private $list;
    private $other;
    
    public function __construct(PDOLib $lib, DataStructInterface $struct)
    {
        parent::__construct($lib);
        $this->struct = $struct;
    }
    
    public function getEventName()
    {
        return 'createTraceTable';
    }
    
    public function make()
    {
        if ($this->struct->getParam('trace')!='trace') {
            return [];
        }
        // inizializzazione strutture dati
        $this->fieldsTrace = [];
        $this->list = [];
        $this->other = [];
        
        // elaborazione
        $this->makeFields($this->struct);
        $this->makePkey($this->struct);
        $this->makeTable($this->struct);
        
        return array_merge($this->list, $this->other);
    }
    
    private function makeFields($struct)
    {
        $addCommand = function ($command)
        {
            $this->addCommand($command);
        };

        foreach ($this->getFieldList($struct) as $name => $field) {
            if ($struct->getParam('trace')) {
                $this->fieldsTrace []= $this->lib->createTableField($struct, $field, 1, $addCommand);
            }
        }
        foreach(self::$operConf as $name => $conf) {
            $this->fieldsTrace []= $this->lib->createTableField($struct, new Field($name, $conf), 1, $addCommand);
        }
    }
    
    private function getFieldList($struct)
    {
        $fieldList = $struct->getFieldList();
        // se è richiesto il trace allora
        // vengono aggiunte le colonne necessarie
        foreach(self::$traceConf as $name => $conf) {
            if (!array_key_exists($name, $fieldList)) {
                $fieldList[$name] = new Field($name, $conf);
            }
        }
        return $fieldList;
    }
    private function makePkey($struct)
    {
        $trace = $struct->getParam('trace');
        
        if ($pkey = $struct->getPkey()) {
            $this->fieldsTrace[] = 'PRIMARY KEY (id__)';
            $this->addCommand($this->lib->createTableIndex($struct->getName().'__', '', $pkey));
        }
    }
    
    private function makeTable($struct)
    {
        $this->list [] = "CREATE TABLE {$struct->getName()}__ (\n".implode(",\n", $this->fieldsTrace).")";
    }
    
    private function addCommand($command)
    {
        $this->other []= $command;
    }
}
