<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Command;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datastruct\Field;
use Spinit\Datamanager\PDO\Command;
use Spinit\Datamanager\PDO\PDOLib;

/**
 * Description of CreateTable
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CreateTable extends Command
{
    private $fields;
    private $list;
    private $other;
    
    public function __construct(PDOLib $lib, DataStructInterface $struct)
    {
        parent::__construct($lib);
        $this->struct = $struct;
    }
    
    public function getEventName()
    {
        return 'createTable';
    }
    
    public function make()
    {
        // inizializzazione strutture dati
        $this->fields = [];
        $this->list = [];
        $this->other = [];
        
        // elaborazione
        $this->makeFields($this->struct);
        $this->makePkey($this->struct);
        $this->makeTable($this->struct);
        $this->makeIndex($this->struct);
        
        return array_merge($this->list, $this->other);
    }
    
    private function makeFields($struct)
    {
        $addCommand = function ($command)
        {
            $this->addCommand($command);
        };

        foreach ($this->getFieldList($struct) as $name => $field) {
            $this->fields []= $this->lib->createTableField($struct, $field, 0, $addCommand);
        }
    }
    
    private function getFieldList($struct)
    {
        $fieldList = $struct->getFieldList();
        // se è richiesto il trace allora
        // vengono aggiunte le colonne necessarie
        if ($struct->getParam('trace')) {
            foreach(self::$traceConf as $name => $conf) {
                if (!array_key_exists($name, $fieldList)) {
                    $fieldList[$name] = new Field($name, $conf);
                }
            }
        }
        return $fieldList;
    }
    private function makePkey($struct)
    {
        $trace = $struct->getParam('trace');
        
        if ($pkey = $struct->getPkey()) {
            $this->fields[] = 'PRIMARY KEY ('.implode(',', $pkey).')';
        }
    }
    
    private function makeTable($struct)
    {
        // creazione indici
        $this->list = ["CREATE TABLE {$struct->getName()} (\n".implode(",\n", $this->fields).")"];
    }
    
    private function makeIndex($struct)
    {
        foreach($struct->getIndexList() as $name => $index) {
            $fieldList = $index->getFieldList();
            if ($fieldList != $struct->getPkey()) {
                $this->addCommand($this->lib->createTableIndex($struct->getName(), $name, $fieldList, $index->getType()));
            }
        }
    }
    
    private function addCommand($command)
    {
        $this->other []= $command;
    }
}

