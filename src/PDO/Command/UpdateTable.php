<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Command;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datamanager\PDO\PDOLib;
use Spinit\Datamanager\PDO\Command;

use Spinit\Datastruct\Field;
use Spinit\Util\TriggerInterface;

/**
 * Description of CreateTable
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class UpdateTable extends Command implements TriggerInterface
{
    private $list;
    private $other;
    private $base;
    private $struct;
    
    use \Spinit\Util\TriggerTrait;
    
    public function __construct(PDOLib $lib, DataStructInterface $base, DataStructInterface $struct)
    {
        parent::__construct($lib);
        $this->base = $base;
        $this->struct = $struct;
        $addCommand = function ($command) {
            $this->other []= $command;
        };
        
        $this->bindExec('insertField', function ($event) use ($addCommand) {
            $this->list[] = $this->lib->addTableField($this->base, $event->getParam(0), 0, $addCommand);
        });
        $this->bindExec('updateField', function ($event) use ($addCommand) {
            if ($this->lib->isSameField($event->getParam(0), $event->getParam(1))) {
                return;
            }
            $this->list []= $this->lib->updateTableField($this->base, $event->getParam(1), 0, $addCommand);
        });
        $this->bindExec('insertIndex', function ($event) use ($addCommand) {
            $index = $event->getParam(0);
            $fieldList = $index->getFieldList();
            if ($fieldList != $this->struct->getPkey()) {
                $addCommand($this->lib->createTableIndex($this->struct->getName(), $index->getName(), $fieldList, $index->getType()));
            }
        });
    }
    
    public function getEventName()
    {
        return 'updateTable';
    }
    
    
    public function make()
    {
        $this->list = [];
        $this->other = [];
        
        if ($this->struct->getParam('trace')) {
            $fieldList = $this->struct->getFieldList();
            foreach(self::$traceConf as $name => $conf) {
                if (!array_key_exists($name, $fieldList)) {
                    $this->struct->addField(new Field($name, $conf));
                }
            }
        }
        $this->base->execDiff($this->struct, $this);
        
        return array_merge($this->list, $this->other);
    }
}
