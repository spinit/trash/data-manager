<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datamanager\PDO;

use Spinit\Datamanager\DataSetInterface;
use Webmozart\Assert\Assert;
use Spinit\Util;

/**
 * Description of PdoDataSet
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDODataSet implements DataSetInterface
{
    /**
     *
     * @var \PDOStatement
     */
    private $rs;
    
    private $current;
    
    private $fetchStyle;
    
    private $open = true;
    
    private $position = -1;
    
    private $columns = null;
    
    public function __construct(\PDOStatement $rs, $fetchStyle = \PDO::FETCH_ASSOC) {
        Assert::notNull($rs);
        $this->rs = $rs;
        $this->fetchStyle = $fetchStyle;
        $this->next();
    }
    public function close()
    {
        if ($this->open) {
            $this->open = false;
            $this->rs->closeCursor();
        }
    }

    public function current()
    {
        if ($this->isOpen()) {
            return $this->current;
        }
        return null;
    }
    public function rowCount()
    {
        if ($this->isOpen()) {
            return $this->rs->rowCount();
        }
        return null;
    }
    public function isOpen()
    {
        return $this->open;
    }

    public function key()
    {
        if ($this->valid()) {
            if (isset($this->current['id'])) {
                return array('id' => $this->current['id']);
            } else {
                $kk = array_keys($this->current);
                return [$kk[0]=>$this->current[$kk[0]]];
            }
        }
        return null;
    }

    public function next()
    {
        if ($this->isOpen()) {
            $this->current = $this->rs->fetch($this->fetchStyle);
            if (!$this->valid()) {
                $this->close();
            } else {
                $this->position++;
            }
        }
        return $this->current();
    }

    public function getAll()
    {
        $data = $this->rs->fetchAll($this->fetchStyle);
        if ($this->current()) {
            array_unshift($data, $this->current());
        }
        $this->close();
        return $data;
    }
    public function getList()
    {
        $list = [];
        while($this->current()) {
            $data = $this->current();
            switch (count($data)) {
                case 1:
                    $list[] = array_shift($data);
                    break;
                case 2:
                    $key = array_shift($data);
                    // se una proprietá è presente, non viene sovrascritta
                    if(array_key_exists($key, $list)) break;
                    $list[$key] = array_shift($data);
                    break;
                default:
                    $key = array_shift($data);
                    $list[$key] = $this->current();
            }
            $this->next();
        }
        return $list;
    }
    
    /**
     * Torna il primo record e chiude il recordset.
     * Se viene passato un paramentro ... allora viene ritornato il valore del campo ... se esiste.
     * @return type
     */
    public function first()
    {
        $current = $this->current;
        $this->close();
        $args = func_get_args();
        if (!count($args)) {
            return $current;
        }
        return Util\arrayGetAssert($current, $args[0], 'Field not found : '.$args[0]);
    }
    public function position()
    {
        return $this->position;
    }

    public function rewind()
    {
    }

    public function valid()
    {
        return !!$this->current;
    }

    public function getMetadata($type = false)
    {
        $info = [];
        foreach(range(0, $this->rs->columnCount() - 1) as $column_index)
        {
            $meta = $this->rs->getColumnMeta($column_index);
            $info[$meta['name']] = ['name'=>$meta['name'], 'label'=>$meta['name'], 'type'=>$this->getType($meta)];
        }
        return $info;
    }
    protected function getType($meta)
    {
        switch($meta['native_type']) {
            default:
                return $meta['native_type'];
        }
    }
}
