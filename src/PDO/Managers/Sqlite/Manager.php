<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Managers\Sqlite;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datamanager\PDO\PDOManager;
use Spinit\Datamanager\PDO\Managers\Sqlite\Lib;
use Spinit\Util;

/**
 * Description of MysqlManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Manager extends PDOManager
{
    public function __construct($info, $params = array())
    {
        parent::__construct($info, $params);
        $this->setLib(new Lib());
    }

    protected function newPDO()
    {
        $fname  = $this->getInfo(1);
        $engine = $this->getInfo(2);
        
        $strcn = "{$this->getInfo(0)}:";
        if ($fname) {
            $strcn .= $fname;
        } else {
            $strcn .= ":{$this->getInfo(2)}:";
        }
        $pdo = new \PDO($strcn);
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $pdo->sqliteCreateFunction('md5', function ($str) {return md5($str); }, 1);
        
        $pdo->info = ['charset' => '', 'engine'=>$this->getInfo(2)];
        return $pdo;
    }

}