<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Managers\Pgsql;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datamanager\PDO\PDOManager;
use Spinit\Datamanager\PDO\Managers\Pgsql\Lib;
use Spinit\Util;

/**
 * Description of MysqlManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Manager extends PDOManager
{
    public function __construct($info, $params = array()) {
        parent::__construct($info, $params);
        $this->setLib(new Lib());
    }

    protected function newPDO()
    {
        $opt = array();
        $arHost = explode('#', $this->getInfo(1));
        list($host, $port) = [array_shift($arHost), Util\nvl(array_shift($arHost),'5432')];
        $nameDB     = $this->getInfo(2);
        $schema     = $this->getInfo(3);
        $username   = $this->getInfo(4);
        $password   = $this->getInfo(5);
        $create     = $this->getInfo(6);
        $engine     = mb_strtoupper($this->getInfo(7));
        
        $strcn = "pgsql:host={$host};port={$port};dbname={$nameDB};";
        $pdo = new \PDO($strcn, $username, $password, $opt);
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $pdo->info = ['charset' => '', 'engine'=>$engine];
        $pdo->exec('SET search_path TO '.$schema);
        $this->setSchema($pdo->schema = $schema);
        return $pdo;
    }

    public function setSchema($name)
    {
        if ($this->getPDO()) {
            $this->getPDO()->exec('SET search_path TO '.$name);
            $this->getPDO()->schema = $name;
        }
        parent::setSchema($name);
    }

}