<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO;

use Spinit\Datamanager\DataManager;
use Spinit\Datastruct\DataStructInterface;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Util;
use Webmozart\Assert\Assert;

/**
 * Description of PDOManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class PDOManager extends DataManager
{
    private $lib = null;
    private $pdo = null;
    private $sqlList = [];
    private $sqlLast = [];
        
    abstract protected function newPDO();

    public function __construct($info, $params = array())
    {
        parent::__construct($info, $params);
        $this->bindExec('createTable', function($event) {
            $struct = $event->getParam(0);
            // se è impostato una directory da dove caricare i dati ..
            if ($root = $this->getParam('xmlroot')) {
                $this->getLib()->loadXmlData($root, $struct);
            }
        });
        
        // gestione inserimento ... e tracing
        $insertFunc = function($resource, $values, $dataArg) {
            $cmd = "INSERT INTO {$resource} (".implode(', ', array_keys($values)).") VALUES (".implode(', ', $values).")";
            return $this->exec($cmd, $dataArg);
        };
        $this->bindExec('insert', function($event) use ($insertFunc) {
            $insertFunc($event->getParam(0), $event->getParam(1), $event->getParam(2));
            return $event->getParam(3);
        });
        $this->bindExec('trace', function($event) use ($insertFunc) {
            $insertFunc($event->getParam(0), $event->getParam(1), $event->getParam(2));
            return $event->getParam(3);
        });
        
        // gestione cancellazione
        $this->bindExec('update', function($event) {
            $resource = $event->getParam(0);
            $sets = $event->getParam(1);
            $whre = $event->getParam(2);
            $data = $event->getParam(3);
            $cmd = "UPDATE {$resource} SET ".implode(', ', $sets)." WHERE ".implode(' AND ', $whre);
            $this->getLib()->notNormalizeNull();
            return $this->exec($cmd, $data);
        });
        
        // gestione cancellazione
        $this->bindExec('delete', function($event) {
            $resource = $event->getParam(0);
            $whre = $event->getParam(1);
            $data = $event->getParam(2);
            $cmd = "DELETE FROM {$resource} WHERE ".implode(' AND ', $whre);
            $this->getLib()->notNormalizeNull();
            return $this->exec($cmd, $data);        
        });
    }
    public function setLib(PDOLib $lib)
    {
        $lib->setManager($this);
        $this->lib = $lib;
        return $this;
    }
    public function getLib()
    {
        Assert::notNull($this->lib, "Occorre istanziare la libreria di riferimento per il manager ".get_class($this));
        return $this->lib;
    }
    public function getPDO()
    {
        return $this->pdo;
    }
    public function setPDO(\PDO $pdo)
    {
        $this->pdo = $pdo;
        return $this;
    }
    
    public function connect()
    {
        if (!$this->pdo) {
            $this->setPDO($this->newPDO());
        }
    }
    public function close()
    {
        $this->sqlLast = [];
        $this->sqlLast = [];
        $this->pdo = null;
    }
    public function serialize()
    {
        $this->close();
        return parent::serialize();
    }
    public function unserialize($serialized)
    {
        parent::unserialize($serialized);
        $this->connect();
    }
    
    /**
     * 
     * @param DataStructInterface $struct
     */
    public function align(DataStructInterface $struct)
    {
        $this->connect();
        $updater = $this->getLib()->getTableUpdater($struct);
        $updater->exec();
        // il dataStruct viene avvisato del tipo di aggiornamento
        if (method_exists($struct, 'trigger')) {
            $struct->trigger($updater->getEventName());
        }
        $updater = $this->getLib()->getTableTraceUpdater($struct);
        $updater->exec();
    }

    public function check($name)
    {
        return $this->getLib()->checkTable($name);
    }
    public function drop($name)
    {
        return $this->getLib()->dropTable($name);
    }
    public function find($resource, $fields, $pkeyList)
    {
        $this->connect();
        $resource = trim($resource);
        $data = is_array($pkeyList) ? $pkeyList : ['id'=>$pkeyList];
        $whre = [];
        $fields = Util\asArray($fields, ',');
        $struct = $this->getDataStruct($resource);
        $select = [];
        foreach($struct->getFieldList() as $field) {
            if (in_array('*', $fields) or in_array($field->getName(), $fields)) {
                $select []= $this->getLib()->formatFieldSelect($field);
            }
            if (array_key_exists(strtolower($field->getName()), $data)) {
                $value = $this->getLib()->formatFieldValue($field, $data);
                if ($value == 'NULL') {
                    $whre []= $field->getName() . ' IS ' . $value;
                } else {
                    $whre []= $field->getName() . ' = ' . $value;
                }
            }
        }
        $query = "SELECT ".implode(', ',$select)."\n".
                 "FROM {$resource}\n".
                 "WHERE ".implode(' AND ', $whre);
        return $this->load($query, $data);
    }
    
    /**
     * restituisce il primo record che ritorna il find.
     * @param type $resource
     * @param type $pkey
     * @param type $fields : se impostato ritorna solo i campi indicati
     * @return type
     */
    public function first($resource, $pkey, $fields = '*')
    {
        return $this->find($resource, $fields, $pkey)->first();
    }
    
    public function getDataStruct($name)
    {
        return $this->getLib()->getDataStruct($name);
    }
    
    public function insert($info, $data, $raw = false, $observer = null)
    {
        $this->connect();
        $data = (array)$data;
        // nome tabella
        $resource = $this->getResourceInfo($info, 'name');
        // struttura tabella
        $struct = $this->getDataStruct($resource);
        $pkey = [];
        // se è previsto un qualche livello di tracciamento ...
        if (!$raw and $this->getResourceInfo($info, 'trace')) {
            $data['dat_ins__'] = $this->getLib()->getNowDataTime();
            $data['usr_ins__'] = $this->getUserTrace();
            $data['usr_del__'] = $data['dat_del__'] = $data['usr_upd__'] = $data['dat_upd__'] = null;
        }
        // lista dei valori da inserire nel comando
        $values = [];
        foreach($struct->getFieldList() as $field) {    
            // vengono presi in considerazione solo i campi presenti nella struttura
            if (!array_key_exists($field->getName(), $data)) {
                continue;
            }
            $value = $data [$field->getName()];
            if ($field->get('ispkey', '')) {
                $pkey[$field->getName()] = $value;
            }
            if (is_array($value)) {
                $fnc = array_shift($value);
                $data[$field->getName()] = array_shift($value);
                $values [$field->getName()] = $fnc.'('.$this->getLib()->formatFieldValue($field, $data).')';
            } else {
                $values [$field->getName()] = $this->getLib()->formatFieldValue($field, $data);
            }
        }
        // vengono presi da $data solo i campi che effettivamente andranno nella tabella
        $dataArg = [];
        foreach(array_keys($data) as $okField) {
            if (array_key_exists($okField, $values)) {
                $dataArg[$okField] = $data[$okField];
            }
        }
        if (substr($resource,-2) == '__' and $raw) {
            return $this->trigger('trace', [$resource, $values, $dataArg, $pkey]);
        }
        $observer && is_callable($observer) && call_user_func($observer, $resource, $data, $raw);
        $ret = $this->trigger('insert', [$resource, $values, $dataArg, $pkey]);
        return $ret;
    }
    
    /**
     * verifica se i valori presenti nel primo parametro siano presenti nel secondo
     * @param type $data
     * @param type $record
     * @return boolean
     */
    private function diffData($dataList, $record)
    {
        $foundDiff = false;
        // se i dati da salvare sono uguali a qualli già presenti .. STOP
        foreach($dataList as $k=>$v) {
            if ($record[$k] != $v) {
                return true;
            }
        }
        return false;
    }
    
    public function update($info, $dataList, $pkeyList, $raw = false, $observer = null)
    {
        $this->connect();
        $data = [];
        $sets = [];
        $whre = [];
        if (!is_array($pkeyList)) {
            $pkeyList = ['id'=>$pkeyList];
        }
        // Nome tabella dati
        $resource = $this->getResourceInfo($info, 'name');
        
        if ($raw === 'raw') {
            foreach($dataList as $k => $v) {
                if (is_array($v)) {
                    $fnc = array_shift($v);
                    $data['__set__'.$k] = array_shift($v);
                    $sets []   = $k." = {$fnc}(:__set__{$k})";
                } else {
                    $data['__set__'.$k] = $v;
                    $sets []   = $k.' = :__set__'.$k;
                }
            }
            foreach($pkeyList as $k => $v) {
                if (is_array($v)) {
                    $fnc = array_shift($v);
                    $data['__whr__'.$k] = array_shift($v);
                    $whre []   = $k." = {$fnc}(:__whr__{$k})";
                } else {
                    if ($v === null) {
                        $whre []   = $k.' IS NULL';
                    } else {
                        $data['__whr__'.$k] = $v;
                        $whre []   = $k.' = :__whr__'.$k;
                    }
                }
            }
        } else {
            // struttura tabella
            $struct = $this->getDataStruct($resource);
            
            if (!$raw or $raw == 'delete') {
                // record corrent (pre-modifica)
                if (!$raw) {
                    $record = $this->first($resource, $pkeyList);
                    if (!$this->diffData($dataList, $record)) {
                        return false;
                    }

                    if ($this->getResourceInfo($info, 'trace')) {
                        $dataList['dat_upd__'] = $this->getLib()->getNowDataTime();
                        $dataList['usr_upd__'] = $this->getUserTrace();
                        $dataList['dat_del__'] = null;
                        $dataList['usr_del__'] = null;
                        if ($this->getResourceInfo($info, 'trace') == 'trace') {
                            $suffix = '__';
                            $record['id__'] = $this->getCounter()->next();
                            $record['dat_opr__'] = $this->getLib()->getNowDataTime();
                            $record['usr_opr__'] = $this->getUserTrace();
                            $this->insert($resource.$suffix, $record, true);
                        }
                    }
                }
            }
            foreach($dataList as $k => $v) {
                if (is_array($v)) {
                    $fnc = array_shift($v);
                    $data['__set__'.$k] = array_shift($v);
                    $sets []   = $k." = {$fnc}(".$this->getLib()->formatFieldValue($struct->getField($k), $data, '__set__').")";
                } else {
                    $data['__set__'.$k] = $v;
                    $sets []   = $k.' = '.$this->getLib()->formatFieldValue($struct->getField($k), $data, '__set__');
                }
            }
            foreach(is_array($pkeyList) ? $pkeyList : ['id'=>$pkeyList] as $k => $v) {
                if (is_array($v)) {
                    $fnc = array_shift($v);
                    $data['__whr__'.$k] = array_shift($v);
                    $whre []   = $k." = {$fnc}(".$this->getLib()->formatFieldValue($struct->getField($k), $data, '__whr__').")";
                } else {
                    $data['__whr__'.$k] = $v;
                    $format = $this->getLib()->formatFieldValue($struct->getField($k), $data, '__whr__');
                    if ($format == 'NULL') {
                        $whre []   = $k.' IS NULL';
                    } else {
                        $whre []   = $k.' = '.$format;
                    }
                }
            }
        }
        $observer && is_callable($observer) && call_user_func($observer, $resource, $dataList, $pkeyList, $raw);
        $ret = $this->trigger('update', [$resource, $sets, $whre, $data]);
        return $ret;
    }
    public function getUserTrace()
    {
        $this->connect();
        return $this->getParam('userTrace') ? str_pad($this->getParam('userTrace'), 32, '0', STR_PAD_LEFT) : null;
    }
    public function delete($info, $pkeyList, $soft = false, $observer = null)
    {
        $this->connect();
        $resource = $this->getResourceInfo($info, 'name');
        
        $data = is_array($pkeyList) ? $pkeyList : ['id'=>$pkeyList];
        
        if ($soft or $this->getResourceInfo($info, 'trace')=='1') {
            // update RAW sui campi (cioè non viene effettuata nessuna valutazione sulla modifica e sul tracing)
            return $this->update(
                $resource, 
                ['dat_del__'=>$this->getLib()->getNowDataTime(), 'usr_del__'=>$this->getUserTrace()], 
                $pkeyList,
                'delete',
                $observer
            );
        }
        
        if ($this->getResourceInfo($info, 'trace') == 'trace') {
            $record = $this->first($resource, $pkeyList);
            $record['id__'] = $this->getCounter()->next();
            $record['is_del__'] = '1';
            $record['dat_opr__'] = $this->getLib()->getNowDataTime();
            $record['usr_opr__'] = $this->getUserTrace();
            $suffix = '__';
            $this->insert($resource.$suffix, $record, true, $observer);
        }
        
        $whre = [];
        $struct = $this->getDataStruct($resource);
        foreach($struct->getFieldList() as $field) {
            if (array_key_exists(strtolower($field->getName()), $data)) {
                $format = $this->getLib()->formatFieldValue($field, $data);
                if ($format == 'NULL') {
                    $whre []= $field->getName() . ' IS NULL';
                } else {
                    $whre []= $field->getName() . ' = ' . $format;
                }
            }
        }
        
        $observer && is_callable($observer) && call_user_func($observer, $resource, $pkeyList, $soft);
        $ret = $this->trigger('delete', [$resource, $whre, $data]);
        return $ret;
    }

    /**
     * Utility
     * @param type $value
     * @return type
     */
    final protected function mapArrayInString($value)
    {
        if (is_array($value)) {
            return json_encode($value, JSON_UNESCAPED_UNICODE);
        }
        return $value;
    }

    /**
     * Esecuzione dello script normalizzandolo rispetto ai parametri passati
     * @param type $sql
     * @param type $params
     * @return type
     */
    public function exec($sql, $params = array(), $args = array(), $info = array(), $vars = array())
    {
        $this->connect();
        $oparams = $params;
        try {
            list($sql, $params) = $this->getLib()->normalizeExec($sql, $params, $args, $info, $vars);
            $this->sqlLast = ['sql' => trim($sql), 'param'=>$params];
            $start = \time();
            if (count($params)) {
                $rs = $this->getPDO()->prepare($sql);
                $res = $rs->execute(array_map([$this, 'mapArrayInString'], (array) $params));
            } else {
                $res = $this->getPDO()->exec($sql);
            }
            $this->sqlLast['time'] = \time() - $start;
            if (Util\getenv('DEBUG')) {
                $this->sqlList[] = $this->sqlLast;
            }
            return $res;
        } catch (\Exception $e) {
            throw new \Exception(json_encode(['message'=>$e->getMessage(), 'sql'=>$sql, 'param'=>$oparams], JSON_PRETTY_PRINT));
        }
    }

    /**
     * Esecuzione della query normalizzandola rispetto ai parametri passati
     * @param type $sql
     * @param type $params
     * @return \Spinit\Datasource\Adapter\Database\PDO\PDODataSet
     */
    public function load($sql, $params = array(), $args = array(), $info = array(), $vars = array())
    {
        $this->connect();
        $oparams = $params;
        $rs = false;
        $sqlraw = $sql;
        try {
            list($sql, $params) = $this->getLib()->normalizeExec($sql, $params, $args, $info, $vars);
            $this->sqlLast = ['sql' => trim($sql), 'param'=>$params];
            if (count($params)) {
                $rs = $this->getPDO()->prepare($sql);
                $rs->execute(array_map([$this, 'mapArrayInString'], (array) $params));
            } else {
                $rs = $this->getPDO()->query($sql);
            }
            $start = \time();
            $this->sqlLast['time'] = \time() - $start;
            if (Util\getenv('DEBUG')) {
                $this->sqlList[] = $this->sqlLast;
            }
        } catch(\Exception $e) {
            $info = ['message'=>$e->getMessage(), 'sql'=>$sql, 'param'=>$oparams, 'trace'=>$e->getTrace()[0]];
            throw new \Exception(json_encode($info, JSON_PRETTY_PRINT));
        }
        Assert::notSame($rs, false);
        $ds = $this->getLib()->getDataSet($rs);
        Assert::isInstanceOf($ds, "\\Spinit\\Datamanager\\PDO\\PDODataSet");
        return $ds;
    }
    
    public function getCommandList($reset = false)
    {
        $result = $this->sqlList;
        if ($reset) {
            $this->sqlList = [];
        }
        return $result;
    }
    
    public function getCommandLast()
    {
        return $this->sqlLast;
    }
    public function quote($str)
    {
        return $this->pdo->quote($str);
    }
    
    public function getLastID()
    {
        return $this->pdo->lastInsertId();
    }
}
