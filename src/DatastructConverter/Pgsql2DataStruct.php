<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\DatastructConverter;

use Spinit\Datastruct\ConverterInterface;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Util;
use Webmozart\Assert\Assert;

use Spinit\Util\Error\NotFoundException;
/**
 * Description of Mysql2DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Pgsql2DataStruct implements ConverterInterface
{
    private $schema;
    private $pdo;
    
    public function __construct($connectionString)
    {
        if ($connectionString instanceof \PDO) {
            $this->pdo = $connectionString;
            Assert::notNull($this->pdo->schema);
            $this->schema = $this->pdo->schema;
            return;
        }
        $this->connectionString = $connectionString;
        $info = explode(':', $connectionString);
        if (in_array($info[0], ['pgsql'])) {
            array_shift($info);
        }
        
        list($hostPortStr, $nameDB, $username, $password) = 
            [array_shift($info), array_shift($info), array_shift($info), array_shift($info)];
        
        $hostPort = explode('#', $hostPortStr);
        
        // impostazione
        $host = array_shift($hostPort);
        $port = Util\nvl(array_shift($hostPort), '3306');
        
        $pdo = new \PDO("pgsql:host={$host};port={$port};database={$nameDB};", $username, $password, $opt);
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $this->schema = 'public';
        $this->pdo = $pdo;
    }
    
    public function getResourceList()
    {
        $sql = "select table_name from information_schema.tables where table_schema = :schema";
        $params = ['schema'=>$this->schema];
        $rs = $this->pdo->prepare($sql);
        $rs->execute($params);
        while($row = $rs->fetch(\PDO::FETCH_ASSOC)) {
            yield ($row['tabl_name']);
        }
        $rs->closeCursor();
    }
    
    public function getDataStruct($table)
    {
        $schema = $this->schema;
        
        if (strpos($table, '.') !== false) {
            list($schema, $table) = explode('.', $table);
        } else {
            //throw new \Exception('bho');
        }
        $ds = new DataStruct($table);
        $sql = "
            SELECT  
                f.attnum AS number,  
                f.attname AS name,  
                f.attnum,  
                CASE WHEN f.attnotnull THEN 't' ELSE '' END AS notnull,  
                pg_catalog.format_type(f.atttypid,f.atttypmod) AS type,  
                CASE  
                    WHEN p.contype = 'p' THEN 't'  
                    ELSE 'f'  
                END AS primarykey,  
                CASE  
                    WHEN p.contype = 'u' THEN 't'  
                    ELSE 'f'
                END AS uniquekey,
                CASE
                    WHEN p.contype = 'f' THEN g.relname
                END AS foreignkey,
                CASE
                    WHEN p.contype = 'f' THEN p.confkey
                END AS foreignkey_fieldnum,
                CASE
                    WHEN p.contype = 'f' THEN g.relname
                END AS foreignkey,
                CASE
                    WHEN p.contype = 'f' THEN p.conkey
                END AS foreignkey_connnum,
                CASE
                    WHEN f.atthasdef = 't' THEN d.adsrc
                END AS default
            FROM pg_attribute f  
                JOIN pg_class c ON c.oid = f.attrelid  
                JOIN pg_type t ON t.oid = f.atttypid  
                LEFT JOIN pg_attrdef d ON d.adrelid = c.oid AND d.adnum = f.attnum  
                LEFT JOIN pg_namespace n ON n.oid = c.relnamespace  
                LEFT JOIN pg_constraint p ON p.conrelid = c.oid AND f.attnum = ANY (p.conkey)  
                LEFT JOIN pg_class AS g ON p.confrelid = g.oid  
            WHERE c.relkind = 'r'::char  
                AND n.nspname = '{$schema}'  -- Replace with Schema name  
                AND c.relname = '{$table}'  -- Replace with table name  
                AND f.attnum > 0 ORDER BY number";
        foreach($this->pdo->query($sql) as $rec) {
            $type = Util\arrayGet($rec, 'type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?( (\w+))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $type = $LVar[1][0];
            $size = $LVar[3][0];
            switch($type) {
                case 'character varying':
                    $type = 'varchar';
                    break;
                case 'binary':
                    if ($size == '16') {
                        list($type, $size) = ['uuid', ''];
                    }
                    break;
                case 'bigint':
                    if ($LVar[5][0] == 'unsigned' and Util\arrayGet($rec, 'Extra') == 'auto_increment') {
                        list($type, $size) = ['increment', ''];
                    }
                    break;
            }
            
            $field = $ds->addField(new Field($rec['name']))
                    ->set('type', $type)
                    ->set('size', $size)
                    ->set('unsigned', $LVar[5][0]=='unsigned')
                    ->set('autoinc', substr($rec['default'],0,7) == 'nextval')
                    ->set('ispkey', Util\arrayGet($rec, 'primarykey') == 't' ? true : false)
                    ->set('notnull', $rec['notnull'] == 't')
                    ->set('default', $rec['default']);
            
            if (Util\arrayGet($rec, 'primarykey')=='t') {
                $ds->addPkeyField($rec['name']);
            }
        }
        /*
        foreach($this->pdo->query("SHOW INDEX FROM {$table}") as $rec) {
            try {
                $index = $ds->getIndex($rec['Key_name']);
            } catch (NotFoundException $ex) {
                $index = $ds->addIndex(new Index($rec['Key_name'], $rec['Index_type']));
            }
            $index->addField($rec['Column_name']);
        }
         * 
         */
        return $ds;
    }
    
    public function getDataStructList() {
        $list = [];
        foreach($this->getResourceList() as $resource) {
            $list[$resourse] = $this->getDataStruct($resource);
        }
        return $list;
    }
}
