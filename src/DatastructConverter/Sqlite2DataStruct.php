<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\DatastructConverter;

use Spinit\Datastruct\ConverterInterface;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Util;
use Webmozart\Assert\Assert;

use Spinit\Util\Error\NotFoundException;
/**
 * Description of Mysql2DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Sqlite2DataStruct implements ConverterInterface
{
    private $pdo;
    
    public function __construct($connectionString)
    {
        if ($connectionString instanceof \PDO) {
            $this->pdo = $connectionString;
            return;
        }
        $this->connectionString = $connectionString;
        $info = explode(':', $connectionString);
        if (in_array($info[0], ['sqlite'])) {
            array_shift($info);
        }
        
        list($fname, $engine) = [array_shift($info), array_shift($info)];
        
        $pdo = new \PDO("sqlite:{$fname}:{$engine}:");
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $this->pdo = $pdo;
    }
    
    public function getResourceList()
    {
        $sql = "SELECT name FROM sqlite_master WHERE type = 'table'";
        $rs = $this->pdo->prepare($sql);
        $params = [];
        $rs->execute($params);
        while($row = $rs->fetch(\PDO::FETCH_ASSOC)) {
            yield ($row['name']);
        }
        $rs->closeCursor();
    }
    
    public function getDataStruct($table)
    {
        $ds = new DataStruct($table);
        $fieldCount = 0;
        foreach($this->pdo->query("PRAGMA table_info('{$table}')") as $rec) {
            $fieldCount += 1;
            $type = Util\arrayGet($rec, 'type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $field = $ds->addField(new Field($rec['name']))
                    ->set('type', $LVar[1][0])
                    ->set('size', $LVar[3][0])
                    ->set('notnull', $rec['notnull']=='1'?'1':'')
                    ->set('default', $rec['dflt_value'])
                    ->set('incval','');
            if ($field->get('type') == 'varchar') {
                if ($field->get('default') == 'NULL') {
                    $field->set('default', null);
                } else if (substr($field->get('default'), 0, 1) == "'") {
                    $field->set('default', substr(str_replace("''", "'", $field->get('default')), 1, -1));
                }
            }
            if ($field->get('type')=='datetime' and $field->get('default')) {
                $field->set('default',"({$field->get('default')})");
            }
            if ($field->get('type')=='binary' and $field->get('size')=='16') {
                $field->set('type', 'uuid');
                $field->set('size','');
            }
            if (Util\arrayGet($rec, 'pk')=='1') {
                $ds->addPkeyField($rec['name']);
            }
        }
        if (!$fieldCount) {
            throw new NotFoundException('Table non found : '.$table);
        }
        // TODO SHOW INDEX
        return $ds;
    }
    
    public function getDataStructList() {
        $list = [];
        foreach($this->getResourceList() as $resource) {
            $list[$resourse] = $this->getDataStruct($resource);
        }
        return $list;
    }
}
