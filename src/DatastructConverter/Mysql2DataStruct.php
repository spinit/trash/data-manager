<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\DatastructConverter;

use Spinit\Datastruct\ConverterInterface;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Util;
use Webmozart\Assert\Assert;

use Spinit\Util\Error\NotFoundException;
/**
 * Description of Mysql2DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Mysql2DataStruct implements ConverterInterface
{
    private $schema;
    private $pdo;
    
    public function __construct($connectionString)
    {
        if ($connectionString instanceof \PDO) {
            $this->pdo = $connectionString;
            Assert::notNull($this->pdo->schema);
            $this->schema = $this->pdo->schema;
            return;
        }
        $this->connectionString = $connectionString;
        $info = explode(':', $connectionString);
        if (in_array($info[0], ['mysql', 'mariadb', 'mysqli'])) {
            array_shift($info);
        }
        
        list($hostPortStr, $nameDB, $username, $password) = 
            [array_shift($info), array_shift($info), array_shift($info), array_shift($info)];
        
        $hostPort = explode('#', $hostPortStr);
        
        // impostazione
        $host = array_shift($hostPort);
        $port = Util\nvl(array_shift($hostPort), '3306');
        
        $opt = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4");
        
        $pdo = new \PDO("mysql:host={$host};port={$port};", $username, $password, $opt);
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $pdo->exec("USE `{$nameDB}`");
        
        $this->schema = $nameDB;
        $this->pdo = $pdo;
    }
    
    public function getResourceList()
    {
        $sql = "select table_name from information_schema.tables where table_schema = :schema";
        $params = ['schema'=>$this->schema];
        $rs = $this->pdo->prepare($sql);
        $rs->execute($params);
        while($row = $rs->fetch(\PDO::FETCH_ASSOC)) {
            yield ($row['tabl_name']);
        }
        $rs->closeCursor();
    }
    
    public function getDataStruct($table)
    {
        $ds = new DataStruct($table);
        if (strpos($table, '.') === false) {
            $table = '`'.$this->schema.'`.'.$table;
        } else {
            //throw new \Exception('bho');
        }
        foreach($this->pdo->query("DESCRIBE {$table}") as $rec) {
            $type = Util\arrayGet($rec, 'Type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?( (\w+))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $type = $LVar[1][0];
            $size = $LVar[3][0];
            switch($type) {
                case 'binary':
                    if ($size == '16') {
                        list($type, $size) = ['uuid', ''];
                    }
                    break;
                case 'bigint':
                    if ($LVar[5][0] == 'unsigned' and Util\arrayGet($rec, 'Extra') == 'auto_increment') {
                        list($type, $size) = ['increment', ''];
                    }
                    break;
            }
            
            $field = $ds->addField(new Field($rec['Field']))
                    ->set('type', $type)
                    ->set('size', $size)
                    ->set('unsigned', $LVar[5][0]=='unsigned')
                    ->set('autoinc', $rec['Extra'] == 'auto_increment')
                    ->set('ispkey', Util\arrayGet($rec, 'Key') == 'PRI' ? true : false)
                    ->set('notnull', $rec['Null'] != 'YES')
                    ->set('default', $rec['Default']);
            
            if (Util\arrayGet($rec, 'Key')=='PRI') {
                $ds->addPkeyField($rec['Field']);
            }
            if ($field->get('type')=='binary' and $field->get('size')=='16') {
                $field->set('type', 'uuid');
                $field->set('size','');
            }
        }
        foreach($this->pdo->query("SHOW INDEX FROM {$table}") as $rec) {
            try {
                $index = $ds->getIndex($rec['Key_name']);
            } catch (NotFoundException $ex) {
                $index = $ds->addIndex(new Index($rec['Key_name'], $rec['Index_type']));
            }
            $index->addField($rec['Column_name']);
        }
        return $ds;
    }
    
    public function getDataStructList() {
        $list = [];
        foreach($this->getResourceList() as $resource) {
            $list[$resourse] = $this->getDataStruct($resource);
        }
        return $list;
    }
}
