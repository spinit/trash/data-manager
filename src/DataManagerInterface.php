<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager;

use Spinit\Datastruct\DataStructInterface;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataManagerInterface
{
    public function align(DataStructInterface $struct);
    public function check($name);
    public function drop($name);
    
    public function exec($query, $param = array(), $args = array(), $info = array());
    public function load($query, $param = array(), $args = array(), $info = array());
    
    public function find($resource, $fields, $pkey);
    public function first($resource, $pkey, $fields = '');
    public function insert($resource, $data);
    public function update($resource, $data, $pkey);
    public function delete($resource, $pkey);
    
    public function setSchema($name);
    public function getSchema();
}
