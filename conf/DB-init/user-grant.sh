if [ "$MYSQL_USER" -a "$MYSQL_PASSWORD" ]; then 
        echo "GRANT ALL ON *.* TO '$MYSQL_USER'@'%' ;" | "${mysql[@]}"
        echo "GRANT GRANT OPTION ON *.* TO '$MYSQL_USER'@'%' ;" | "${mysql[@]}"
        echo 'FLUSH PRIVILEGES ;' | "${mysql[@]}"
fi    
